Laravel Homestead phpMyAdmin Installation
==========================
***
Read `after.sh`, `Homestead.yaml.example` files and check `vagrant` folder.

***
### Install dependencies
```
composer install
```

***
### Create Homestead.yaml
on Windows
```
vendor\\bin\\homestead make
```
on Linux
```
php vendor/bin/homestead make
```

***
### Configure Homestead.yaml

File `Homestead.yaml` will be generated based on `Homestead.yaml.example`  
**Important** your project folder needs to be mapped to `/app`
```
ip: 192.168.10.10
memory: 2048
cpus: 1
provider: virtualbox
mariadb: true
authorize: ~/.ssh/id_rsa.pub
keys:
    - ~/.ssh/id_rsa
folders:
    -
        map: 'C:\your-path\project'
        to: /app
sites:
    -
        map: project.local
        to: /app/public
    -
        map: phpmyadmin.local
        to: /usr/share/phpmyadmin
databases:
    - mydb
name: project
hostname: project
```

***
### Generate ssh key

```
cd ~/.ssh
ssh-keygen -t rsa -b 4096 -C "vagrant key"
```

***
### Set project database name

Replace `mydb ` with your database name
```
databases:
    - mydb
```
Read **Prepare vagrant folder content** about restoring database from sql file

***
### Configure hosts file (for Windows users)

Amend file `C:\Windows\System32\Drivers\etc\hosts`  
  
```
192.168.10.10 project.local
192.168.10.10 phpmyadmin.local
```

***
### Prepare vagrant folder content
In order for phpMyAdmin to be installed `vagrant` folder must contain:  
- `config.inc.php` (included in source)  
- optional phpMyAdmin tar.gz installation file ( for example `phpMyAdmin-4.8.3-all-languages.tar.gz`)  
  If file with extension `.tar.gz` is not found, the latest version will be downloaded from https://www.phpmyadmin.net/downloads  

If file with `sql` extension exists it will be restored to database named as file name.  
Sample file `mydb_restore.sql` is included in source. You can remove it.

***
### Launch vagrant
```
vagrant up
```

***
### Other useful Vagrant commands

```
vagrant up
vagrant halt
vagrant reload
vagrant provision
vagrant status
vagrant port
vagrant ssh
vagrant ssh-config
vagrant destroy project
vagrant box list
vagrant box remove laravel/homestead
vagrant global-status
vagrant destroy 1a2b3c4d
vagrant snapshot save project mysnapshot
vagrant snapshot restore project mysnapshot
vagrant snapshot delete mysnapshot
```

***
### Project links

Project  
http://project.local

phpMyAdmin  
http://phpmyadmin.local  
(database credentials - username: homestead, password: secret)

Mailhog  
http://localhost:8025