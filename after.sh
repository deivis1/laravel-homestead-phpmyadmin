#!/bin/bash

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.
#
# If you have user-specific configurations you would like
# to apply, you may also create user-customizations.sh,
# which will be run after this script.

adminSrc=`ls /app/vagrant/phpMyAdmin*.tar.gz 2> /dev/null`
adminDest=/usr/share/phpmyadmin
sqlFile=`ls /app/vagrant/*.sql 2> /dev/null | head -n 1`

generateSecret() {
  cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-32} | head -n 1
}

if [ -d "$adminDest" ]; then
  echo "Removing old installation"
  sudo rm -rf $adminDest
fi

if [ "$adminSrc" = "" ] && [ -f "/app/vagrant/config.inc.php" ] && [ ! -d "$adminDest" ]; then
  echo "Downloading latest version"
  downloadFile=$(wget -qO- 'https://www.phpmyadmin.net/downloads' | grep -oP 'https://[/A-z0-9+-.]*tar\.gz' | head -n 1)
  wget $downloadFile -P /app/vagrant
  adminSrc=`ls /app/vagrant/phpMyAdmin*.tar.gz 2> /dev/null`
fi

if [ "$adminSrc" != "" ] && [ -f "/app/vagrant/config.inc.php" ]; then

  if [ ! -d "$adminDest" ]; then
    echo "Installing phpMyAdmin"

    sudo mkdir -p $adminDest
    sudo tar xzf $adminSrc -C $adminDest --strip-components=1

    sudo cp /app/vagrant/config.inc.php $adminDest/
    blowfishSecret=$(generateSecret)
    controlSecret=$(generateSecret)

    mysql -uhomestead -psecret < $adminDest/sql/create_tables.sql
    mysql -uhomestead -psecret -e "GRANT ALL PRIVILEGES ON phpmyadmin.* TO 'pma'@'localhost' IDENTIFIED BY '$controlSecret'; FLUSH PRIVILEGES;"

    sudo sed -i "/blowfish_secret/s/ = '.*'/ = '${blowfishSecret}'/" $adminDest/config.inc.php
    sudo sed -i "/controlpass/s/ = '.*'/ = '${controlSecret}'/" $adminDest/config.inc.php

    sudo chmod 755 -R $adminDest
    sudo chmod 644 $adminDest/config.inc.php

    echo "Done"
  else
    echo "Directory $adminDest already exists"
  fi

fi

if [ "$sqlFile" != "" ]; then
  dbName=$(basename $sqlFile .sql)
  echo "Restoring database $dbName"
  mysql -uhomestead -psecret -e "DROP DATABASE IF EXISTS \`$dbName\`;"
  mysql -uhomestead -psecret -e "CREATE DATABASE \`$dbName\` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;"
  mysql -uhomestead -psecret $dbName < $sqlFile
fi

echo -e "\ncd /app" >> /home/vagrant/.profile

echo
echo "Available URLs"
echo "-----------------------------------"
echo "Project     http://project.local"
echo "phpMyAdmin  http://phpmyadmin.local"
echo "Mailhog     http://localhost:8025"
echo "-----------------------------------"
echo